﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilipsLitleHelper.WebAPI.Models
{
    public class Opportunity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Address Location { get; set; }

        public List<Match> Matches { get; set; }
    }
}