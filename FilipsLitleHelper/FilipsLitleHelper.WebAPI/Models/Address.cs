﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilipsLitleHelper.WebAPI.Models
{
    public class Address
    {
        public string Location { get; set; }
        public decimal Longitude { get; set; }
        public decimal Latitude { get; set; }
    }
}