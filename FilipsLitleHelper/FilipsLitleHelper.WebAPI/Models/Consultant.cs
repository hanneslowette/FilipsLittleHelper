﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilipsLitleHelper.WebAPI.Models
{
    public class Consultant
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public Address Address { get; set; }
        public List<string> PreferredTechnologies { get; set; }
        public List<string> AvoidTechnologies { get; set; }
        public List<string> Experiences { get; set; }
        public int MaxTravelTimeMinutes { get; set; }
    }
}