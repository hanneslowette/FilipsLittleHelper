﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilipsLitleHelper.WebAPI.Models
{
    public class Match
    {
        public Consultant Consultant { get; set; }
        public decimal MatchPercentage { get; set; }
    }
}