﻿using System.Collections.Generic;
using System.Web.Http;
using FilipsLitleHelper.WebAPI.Models;

namespace FilipsLitleHelper.WebAPI.Controllers
{
    public class OpportunityController : ApiController
    {
        public Opportunity Get(int id)
        {
            var result = new Opportunity
            {
                Name = "Awesome Opportunity",
                Description = "This is an awesome project where you use all the new hip technologies.",
                Location = new Address
                {
                    Location = "Entrepotkaai 10A, 2000 Antwerpen",
                    Longitude = 51.228743M,
                    Latitude = 4.4094303M
                }
            };

            var matches = new List<Match>
            {
                new Match
                {
                    MatchPercentage = 65,
                    Consultant = new Consultant
                    {
                        Name = "Bram Devuyst",
                        Email = "bram.devuyst@axxes.com",
                        MaxTravelTimeMinutes = 75,
                        PreferredTechnologies = new List<string> {"C#", "Xamarin", "SiteFinity"},
                        AvoidTechnologies = new List<string> {"WPF", "Winforms"},
                        Experiences = new List<string> {"C#", "SiteFinity", "Custom development"},
                        Address = new Address
                        {
                            Location = "Bruneaustraat 90, Kester",
                            Longitude = 50.7629652M,
                            Latitude = 4.1159017M
                        }
                    }
                },
                new Match
                {
                    MatchPercentage = 78,
                    Consultant = new Consultant
                    {
                        Name = "Andy Smet",
                        Email = "andy.smet@axxes.com",
                        MaxTravelTimeMinutes = 90,
                        PreferredTechnologies = new List<string> {"C#", "Winforms", "WPF"},
                        AvoidTechnologies = new List<string> {"WCF", "VBA"},
                        Experiences = new List<string> {"VBA", "C#", "ASP.NET MVC"},
                        Address = new Address
                        {
                            Location = "Keerbaan 17, Wommelgem",
                            Longitude = 51.2130015M,
                            Latitude = 4.5403599M
                        }
                    }
                },
                new Match
                {
                    MatchPercentage = 70,
                    Consultant = new Consultant
                    {
                        Name = "Jensen Somers",
                        Email = "jensen.somers@axxes.com",
                        MaxTravelTimeMinutes = 60,
                        PreferredTechnologies = new List<string> {"Angular", "SpecFlow", "Octopus"},
                        AvoidTechnologies = new List<string> {"Java", "Winforms"},
                        Experiences = new List<string> {"C#", "Angular", "Team Foundation Service"},
                        Address = new Address
                        {
                            Location = "Aarschotsestraat 52, Peutie",
                            Longitude = 50.9252207M,
                            Latitude = 4.4508386M
                        }
                    }
                },
                new Match
                {
                    MatchPercentage = 93,
                    Consultant = new Consultant
                    {
                        Name = "Hannes Lowette",
                        Email = "hannes.lowette@axxes.com",
                        MaxTravelTimeMinutes = 75,
                        PreferredTechnologies = new List<string> {"C#", "ServiceStack", "Nancy"},
                        AvoidTechnologies = new List<string> {"WPF", "Winforms"},
                        Experiences = new List<string> {"C#", "WCF", "WebAPI"},
                        Address = new Address
                        {
                            Location = "Wezelhof 19, Mol",
                            Longitude = 51.1887238M,
                            Latitude = 5.2227679M
                        }
                    }
                }
            };

            result.Matches = matches;

            return result;
        }
    }
}
